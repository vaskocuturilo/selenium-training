package driver;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static utils.PropertiesReader.loadProperty;

/**
 * The type Test runner.
 */
public class TestRunner {

    /**
     * The Driver.
     */
    protected WebDriver driver;


    public TestRunner() {
        super();
        //empty
        return;
    }


    /**
     * Start browser.
     */
    @BeforeTest(alwaysRun = true)
    final public void startBrowser() throws IOException {
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1300, 768));
        driver.get(loadProperty("url"));
    }


    /**
     * Teardown.
     */
    @AfterTest(alwaysRun = true)
    final public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

}


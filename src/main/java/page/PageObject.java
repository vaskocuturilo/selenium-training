package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * The type Page object.
 */
public class PageObject {

    /**
     * The Driver.
     */
    protected WebDriver driver;

    /**
     * Instantiates a new Page object.
     *
     * @param driver the driver
     */
    public PageObject(final WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}

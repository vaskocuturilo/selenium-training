package utils;

import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {

    public static String loadProperty(final String name) throws IOException {

        if (name != null) {
            Properties properties = new Properties();
            properties.load(PropertiesReader.class.getResourceAsStream("/testdata.properties"));
            return properties.getProperty(name);
        }
        return "";
    }
}

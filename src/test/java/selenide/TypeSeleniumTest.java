package selenide;


import driver.TestRunner;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.GoogleSearch;

import java.io.IOException;


public class TypeSeleniumTest extends TestRunner {

    @Test(description = "Validate of Selenium output !=9")
    public void testOpenGooglePageAndTypeSelenium() throws IOException {
        GoogleSearch googleSearch = new GoogleSearch(driver);
        googleSearch.searchByWord("Selenium");
        googleSearch.waitUntilAllPageElementsAreLoaded();
        Assert.assertNotEquals(9, googleSearch.returnListOfOutput());
    }
}



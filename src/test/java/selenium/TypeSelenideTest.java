package selenium;

import driver.TestRunner;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.GoogleSearch;

import java.io.IOException;


public class TypeSelenideTest extends TestRunner {

    @Test(description = "Validate of Selenide output = 9")
    public void testOpenGooglePageAndTypeSelenide() throws IOException {
        GoogleSearch googleSearch = new GoogleSearch(driver);
        googleSearch.searchByWord("Selenide");
        googleSearch.waitUntilAllPageElementsAreLoaded();
        Assert.assertEquals(10, googleSearch.returnListOfOutput());
    }
}
